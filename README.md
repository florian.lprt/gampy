# Gampy

Run Gama simulations from python.

```python
from gampy import Gampy

g = Gampy(source_path='mymodel.gaml', experiment='test', final_step=100)
g.add_parameter(name='foo', value=500, p_type='int')
g.add_output('bar', framerate=10)
g.run('output_dir', gama='path/to/gama/')
```

## Setup

### Clone

First, clone this repository.

```sh
git clone https://gitlab.com/florian.lprt/gampy.git
```

### Gama

Get the latest version of
[Gama platform](https://gama-platform.github.io/download).

### Python virtual environment (recommended)

Get the latest versions of `python3`, `python3-pip` and `python3-venv`.

Then, install the python virtual environment for `gampy`.

```sh
cd gampy
# install gampy virtual environment
python3 -m venv gampy_env
. gampy_env/bin/activate
pip install pkgconfig setuptools
pip install . --upgrade
deactivate
```

You can check everything is fine with the `pip list` command.

```sh
. gampy_env/bin/activate
pip list
Package    Version
---------- -------
gampy      0.1    
lxml       4.2.4  
pip        18.0   
pkgconfig  1.4.0  
setuptools 39.0.1
deactivate
```

## Usage

### Import

In your python script, import `gampy`.

```python
from gampy import Gampy
```

### Gampy class

`gampy` package provides the `Gampy` class, which allows you to configure and to
run your Gama models from a python script.

#### Initialize

Create a new instance of `Gampy`.

```python
g = Gampy(source_path, experiment [, id] [, final_step] [, until])
```

with:
- `source_path`: contains the relative or absolute path to read the `gaml`
  model,
- `experiment`: determines which experiment should be run on the model ; this
  experiment should exist,
- `id`: prefix for output files,
- `final_step`: determines the number of simulation step you want to run,
- `until`: ends the simulation if the condition is met (written in `gaml`).

(See Gama platform
[wiki](https://github.com/gama-platform/gama/wiki/Headless#heading)
for more details)

Example:

```python
g = Gampy(source_path='mymodel.gaml', experiment='test', id=1, final_step=100)
```

#### Add simulation parameters

You can add as many simulation parameter as you need with the following method:

```python
g.add_parameter(name, value, p_type)
```

with:

- `name`: name of the parameter in the gaml model,
- `type`: type of the parameter (`int`, `float`, `boolean`, `string`),
- `value`: the chosen value.

Example:

```python
g.add_parameter('myint', 500, 'int')
g.add_parameter('mystring', 'Hello world!', 'string')
```

(See Gama platform
[wiki](https://github.com/gama-platform/gama/wiki/Headless#parameters)
for more details)

#### Add simulation outputs

You can add as many simulation outputs as you need with the following method:

```python
g.add_ouput(name [, framerate])
```

with:


- `name`: name of the output in the `output/permanent` section in the experiment
  or name of the `experiment/model` attribute to retrieve.
- `framerate`: the frequency of the monitoring (each step, each 2 steps, each 100 steps...) ; default is `1` (each step).

Example:

```python
g.add_output('myoutput_1')
g.add_output('myoutput_2', framerate=10)
```

(See Gama platform
[wiki](https://github.com/gama-platform/gama/wiki/Headless#outputs)
for more details)

#### Run

Once you have configured your `gampy` instance, you can run a simulation with:

```python
g.run(output_dir [, gama] [, memory] [, hpc])
```

with:

- `output_dir`: path to the output directory where to write the results,
- `gama`: path to the gama installation directory,
- `memory`: allocated memory for the simulation (mega, default: `2048`),
- `hpc`: allocated CPU cores for the simulation (default: all).

Example:

```python
g.run('output_results', gama='path/to/gama', memory=4096, hpc=8)
```

### Fully working example

You can find source files of a fully working example of `gampy` in the `example`
directory:

- `example.py`: python script using `gampy`,

```python
from gampy import Gampy

g = Gampy(source_path='test_gama.gaml', experiment='test', final_step=100)
g.add_parameter(name='myparameter', value=500, p_type='int')
g.add_output('mymonitor')
g.run('out_test', gama='path/to/gama/')
```

- `example.gaml`: the Gama model used.

```gaml
model gampyTest

global {
	int foo <- 200;
	init { create bar number: foo; }
	reflex end when: foo <= 0 { do halt; }
}

species bar {
	int x <- rnd(100000);
	reflex half { x <- int(x / 2); }
	reflex end when: x <= 0 { foo <- foo - 1; do die; }
}

experiment test type: gui {
	parameter "myparameter" var: foo;
	output {
		monitor "mymonitor" value: foo;
	}
}
```

- run (*don't forget to activate your python virtual environment !*):

```sh
. gampy_env/bin/activate
python example.py
```
