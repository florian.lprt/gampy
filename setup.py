import pkgconfig
from setuptools import setup

setup(
    name='gampy',
    version='0.1',
    description='Run Gama simulations from python.',
    url='https://gitlab.com/florian.lprt/gampy',
    author='Florian Leprêtre',
    author_email='florian.lprt@gmail.com',
    packages=['gampy'],
    install_requires = ['lxml'],
)
