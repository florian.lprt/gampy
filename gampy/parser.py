'''
    Gampy

    Parsing functions
'''

from lxml import etree

from . import settings as s

# Utils
#########################################

def to_camel_case(snake_str):
    ''' Convert snake case to camel case '''
    words = snake_str.split('_')
    return words[0].lower() + ''.join(_.title() for _ in words[1:])

# XML parsers
#########################################

def list_to_subelements(parent, wrapper_name, sub_names, sub_attrs):
    ''' Parse a list to XML subelements '''
    wrapper = etree.SubElement(parent, wrapper_name)
    for _ in sub_attrs:
        etree.SubElement(wrapper, sub_names, _)

def to_xml(simulation, parameters, outputs):
    ''' Parse simulation, parameters and outputs to XML  '''
    root = etree.Element(s.SIMULATION, simulation)
    list_to_subelements(root, s.PARAMETERS, s.PARAMETER, parameters)
    list_to_subelements(root, s.OUTPUTS, s.OUTPUT, outputs)
    tree = etree.ElementTree(root)
    tree.write(s.SIM_XML, pretty_print=True, xml_declaration=True, encoding="utf-8")
